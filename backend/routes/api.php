<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\MovieController;
use App\Http\Controllers\Api\LikesHatesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::middleware('auth:sanctum')->post('/createmovie', [MovieController::class, 'createMovie']);
Route::middleware('auth:sanctum')->post('/like', [LikesHatesController::class, 'like']);
Route::middleware('auth:sanctum')->post('/hate', [LikesHatesController::class, 'hate']);
Route::middleware('auth:sanctum')->post('/retracting', [LikesHatesController::class, 'retracting']);



Route::get('/getmovies', [MovieController::class, 'getMovies']);
Route::middleware('auth:sanctum')->get('/logout', [UserController::class, 'logout']);




Route::post('/register', [UserController::class, 'register']);
Route::post('/login', [UserController::class, 'login']);

// Route::post('/createmovie', [MovieController::class, 'createmovie']);

// Route::post('/register', function () {
//     return response()->json([
//         'status' => 'success'
//     ], 200);

// });