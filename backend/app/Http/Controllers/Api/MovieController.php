<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Movies;
use DB;


class MovieController extends Controller
{
    //

    public function createMovie(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);
        $user = auth()->user();
        try {
            Movies::create([
                'title' => $request->title,
                'description' => $request->description,
                'user_id' => $user->id
            ]);
        } catch (Exception $e) {

            return response('DB_ERROR ESHOP' . $e, 500);
        }
        return response()->json([
            'message' => 'Ok',
            'username' => $user->name
        ], 200);
    }



    public function getMovies(Request $request)
    {
            $query = Movies::query();

            $query->select(
            'movies.id','movies.title','movies.description','movies.likes',
            'movies.hates','movies.created_at','users.id as userid','users.name as username',
            DB::raw("CASE WHEN likes = (select MAX(likes) from movies) THEN 'true' ELSE 'false' END AS maxlikes"),
            DB::raw("CASE WHEN hates = (select MAX(hates) from movies) THEN 'true' ELSE 'false' END AS maxhates"),
                  
            );

            $query->leftJoin('users', 'users.id', '=', 'movies.user_id');

            if ($request->user('sanctum')) {
                $user = $request->user('sanctum');
                $query->addSelect('likes_hates.reaction');
                $query->leftJoin('likes_hates', function ($join) use ($user) {
                    $join->on('likes_hates.movie_id', '=', 'movies.id');
                    $join->where('likes_hates.user_id', '=', $user->id);
                });

            }
            else{
                $query->addSelect(DB::raw("'null' as reaction"));
            }
            
            if ($request->filled('user')) {
                $userid = $request->user;
                $query->where('users.id', '=', $userid );
            }

            if ($request->filled('filter')) {

                if ($request->filter == "LIKES") {
                    $filter = "likes";
                }
        
                if ($request->filter == "HATES") {
                    $filter = "hates";
                }
        
        
                if ($request->filter == "DATES") {
                    $filter = "created_at";
                }

                
                $query->orderBy($filter, "desc");
            }


            return $query->get();
    }


}
