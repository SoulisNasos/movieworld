<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\LikesHates;
use App\Models\Movies;



class LikesHatesController extends Controller
{
    //
    

    public function like(Request $request)
    {
        $request->validate([
            'movie_id' => 'required',
        ]);
        $user = auth()->user();
        $movie =  Movies::find($request->movie_id);
        if($user->id == $movie->user_id){
            return response()->json([
                'message' => 'USER_OWNER_OF_MOVIE',
            ], 200);
        }

        $likes_hates = LikesHates::where('user_id', $user->id)->where('movie_id', $request->movie_id)->get();

        if($likes_hates->isEmpty()){

            try {
                LikesHates::create([
                    'user_id' => $user->id,
                    'movie_id' => $request->movie_id,
                    'reaction' => 'LIKE'
                ]);
               
               $movie->likes = $movie->likes +1;
               $movie->save();
            } catch (Exception $e) {
                
                return response('DB_ERROR ESHOP' . $e, 500);
            }
    
            return response()->json([
                'message' => 'WAS_ABSENTION',
            ], 200);

        }

        if($likes_hates[0]->reaction == 'LIKE'){
            return response()->json([
                'message' => 'WAS_LIKE',
            ], 200);
        }

        if($likes_hates[0]->reaction == 'HATE'){
            $likes_hates[0]->reaction = 'LIKE';
            $likes_hates[0]->save();
            $movie =  Movies::find($request->movie_id);
            $movie->likes = $movie->likes +1;
            $movie->hates = $movie->hates -1;
            $movie->save();
            return response()->json([
                'message' => 'WAS_HATE',
            ], 200);
        }
    }





    public function hate(Request $request)
    {
        $request->validate([
            'movie_id' => 'required',
        ]);
        $user = auth()->user();
        $movie =  Movies::find($request->movie_id);


        if($user->id == $movie->user_id){
            return response()->json([
                'message' => 'USER_OWNER_OF_MOVIE',
            ], 200);
        }

        $likes_hates = LikesHates::where('user_id', $user->id)->where('movie_id', $request->movie_id)->get();

        if($likes_hates->isEmpty()){

            try {
                LikesHates::create([
                    'user_id' => $user->id,
                    'movie_id' => $request->movie_id,
                    'reaction' => 'HATE'
                ]);
               
               $movie->hates = $movie->hates +1;
               $movie->save();
            } catch (Exception $e) {
                
                return response('DB_ERROR ESHOP' . $e, 500);
            }
    
            return response()->json([
                'message' => 'WAS_ABSENTION',
            ], 200);

        }

        if($likes_hates[0]->reaction == 'HATE'){
            return response()->json([
                'message' => 'WAS_HATE',
            ], 200);
        }

        if($likes_hates[0]->reaction == 'LIKE'){
            $likes_hates[0]->reaction = 'HATE';
            $likes_hates[0]->save();
            $movie =  Movies::find($request->movie_id);
            $movie->likes = $movie->likes -1;
            $movie->hates = $movie->hates +1;
            $movie->save();
            return response()->json([
                'message' => 'WAS_LIKE',
            ], 200);
        }
    }






    public function retracting(Request $request){
        $request->validate([
            'movie_id' => 'required',
        ]);
        $user = auth()->user();
        $movie =  Movies::find($request->movie_id);
        if($user->id == $movie->user_id){
            return response()->json([
                'message' => 'USER_OWNER_OF_MOVIE',
            ], 200);
        }

        $likes_hates = LikesHates::where('user_id', $user->id)->where('movie_id', $request->movie_id)->get();

        if($likes_hates->isEmpty()){

            return response()->json([
                'message' => 'WAS_ABSENTION',
            ], 200);

        }

        if($likes_hates[0]->reaction == 'LIKE'){
            $movie->likes = $movie->likes -1;
            $movie->save();
            $likes_hates[0]->delete();

            return response()->json([
                'message' => 'WAS_LIKE',
            ], 200);
        }

        if($likes_hates[0]->reaction == 'HATE'){
            // $likes_hates[0]->reaction = 'LIKE';
            // $likes_hates[0]->save();

            // $movie->likes = $movie->likes +1;
            $movie->hates = $movie->hates -1;
            $movie->save();
            $likes_hates[0]->delete();
            return response()->json([
                'message' => 'WAS_HATE',
            ], 200);
        }
    }























}
