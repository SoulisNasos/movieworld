<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('likes_hates', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id');
            $table->foreign('user_id')->references('id')->on('users')
            ->onDelete('cascade');
            $table->foreignId('movie_id');
            $table->foreign('movie_id')->references('id')->on('movies')
            ->onDelete('cascade');
            $table->enum('reaction', ['LIKE', 'HATE']);
            $table->timestamps();
            $table->unique(["user_id", "movie_id"], 'user_movie');



        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('likes_hates');
    }
};
