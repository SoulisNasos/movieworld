import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  @ViewChild('authForm') authForm!: NgForm;


  constructor(private http: HttpClient, private router: Router) {

  }


  isLoading = false;
  successShow = false;
  error: string = '';
  isAuthenticated = false;
  errorShow = false;
  data : any;

  onSubmit(){
    this.isLoading = true;
    this.data={
      email : this.authForm.form.value.email,
      password: this.authForm.form.value.password,
      name: this.authForm.value.name
    }
    console.log(this.data);
    this.http
      .post<any>(
        'http://127.0.0.1:9600/api/register', this.data
      )
      .subscribe({
        next: (res) => {
          this.isLoading = false;
          localStorage.setItem('movieworldtoken', res.token);
          localStorage.setItem('movieworldname', res.name);
          this.router.navigate(['/listofmovies']);
        },
        error: (e) => {  this.isLoading = false;
          this.errorShow = true;
          setTimeout(() => { this.errorShow = false }, 3000);
        },
        complete: () => {this.isLoading = false;}  
      });
  }

}
