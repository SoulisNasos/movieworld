import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-movie',
  templateUrl: './new-movie.component.html',
  styleUrls: ['./new-movie.component.css']
})
export class NewMovieComponent {
  @ViewChild('movieForm') movieForm!: NgForm;
  constructor(private http: HttpClient, private router: Router) {

  }

  isLoading = false;
  data : any;
  errorShow = false;
  successShow = false;

  onSubmit(){
    this.isLoading = true;
    this.data={
      title : this.movieForm.form.value.title,
      description: this.movieForm.form.value.description,
    }
    console.log(this.data);
    const headers = { 'Authorization': 'Bearer '+localStorage.getItem("movieworldtoken") }
    this.http
      .post<any>(
        'http://127.0.0.1:9600/api/createmovie', this.data, {headers}
      )
      .subscribe({
        next: (res) => {
          this.isLoading = false;
          this.successShow = true;
          this.movieForm.reset();
          setTimeout(() => { this.successShow = false }, 3000);
          // localStorage.setItem('movieworldtoken', res.token);
          // localStorage.setItem('movieworldname', res.name);
          // // this.router.navigate(['/listofmovies']);
        },
        error: (e) => {  this.isLoading = false;
          this.errorShow = true;
          setTimeout(() => { this.errorShow = false }, 3000);
        },
        complete: () => {this.isLoading = false;}  
      });
  }
}
