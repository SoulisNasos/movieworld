import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-list-movies',
  templateUrl: './list-movies.component.html',
  styleUrls: ['./list-movies.component.css']
})
export class ListMoviesComponent implements OnInit {
  token: string | null | undefined;
  name: string | null | undefined;
  islogin = false;
  movies: any = [];
  total_number_of_movies: number | undefined;

  constructor(private http: HttpClient, private router: Router) {

  }



  ngOnInit(): void {
    this.ifUserIsLogin();
    this.fetchMovies();




  }

  fetchMovies() {
    this.movies = [];
    this.total_number_of_movies = undefined;

    const headers = { 'Authorization': 'Bearer ' + localStorage.getItem("movieworldtoken") }

    this.http
      .get<any>(
        'http://127.0.0.1:9600/api/getmovies', { headers }
      )
      .subscribe({
        next: (res) => {
          for (var i = 0; i < res.length; i++) {
            var data: any = {};
            // console.log('in loop' + res[i].title);
            data["title"] = res[i].title;
            data["description"] = res[i].description;
            data["likes"] = res[i].likes;
            data["hates"] = res[i].hates;
            data["date"] = res[i].created_at;
            data["username"] = res[i].username;
            data["user_id"] = res[i].userid;
            data["id"] = res[i].id;
            data["reaction"] = res[i].reaction;
            data["maxlikes"] = res[i].maxlikes;
            data["maxhates"] = res[i].maxhates;
            // console.log(data);
            this.movies.push(data);
          }
          this.total_number_of_movies = this.movies.length;
          // console.log(this.movies);
        },
        error: (e) => { },
        complete: () => { }
      });

  }

  ifUserIsLogin() {
    this.token = localStorage.getItem("movieworldtoken");
    this.name = localStorage.getItem("movieworldname");
    if (
      (this.token !== null && this.token !== null) &&
      (this.name !== 'undefined' && this.name !== 'undefined') &&
      (this.name !== undefined && this.name !== undefined)
    ) {
      this.islogin = true;
    }

  }


  logout() {


    const headers = { 'Authorization': 'Bearer ' + localStorage.getItem("movieworldtoken") }
    this.http
      .get<any>(
        'http://127.0.0.1:9600/api/logout', { headers }
      )
      .subscribe({
        next: (res) => {

        },
        error: (e) => { },
        complete: () => { }
      });


    localStorage.removeItem('movieworldtoken');
    localStorage.removeItem('movieworldname');
    this.name = null;
    this.token = null;
    this.islogin = false;

  }

  like(movie_id: number) {
    var data: any;
    const headers = { 'Authorization': 'Bearer ' + localStorage.getItem("movieworldtoken") }
    data = {
      movie_id: movie_id
    }
    this.http
      .post<any>(
        'http://127.0.0.1:9600/api/like', data, { headers }
      )
      .subscribe({
        next: (res) => {
          var index = this.movies.findIndex((x: { id: number; }) => x.id == movie_id)
          if (res.message == "WAS_LIKE") {
          }
          else if (res.message == "WAS_HATE") {
            this.movies[index].likes = this.movies[index].likes + 1;
            this.movies[index].hates = this.movies[index].hates - 1;
          }
          else if (res.message == "WAS_ABSENTION") {
            this.movies[index].likes = this.movies[index].likes + 1;
          }
          this.movies[index].reaction = "LIKE";
        },
        error: (e) => {
        },
      });
  }


  hate(movie_id: number) {
    var data: any;
    const headers = { 'Authorization': 'Bearer ' + localStorage.getItem("movieworldtoken") }
    data = {
      movie_id: movie_id
    }
    this.http
      .post<any>(
        'http://127.0.0.1:9600/api/hate', data, { headers }
      )
      .subscribe({
        next: (res) => {
          var index = this.movies.findIndex((x: { id: number; }) => x.id == movie_id)
          if (res.message == "WAS_HATE") {
          }
          else if (res.message == "WAS_LIKE") {
            this.movies[index].likes = this.movies[index].likes - 1;
            this.movies[index].hates = this.movies[index].hates + 1;
          }
          else if (res.message == "WAS_ABSENTION") {
            this.movies[index].hates = this.movies[index].hates + 1;
          }
          this.movies[index].reaction = "HATE";
        },
        error: (e) => {
        },
      });
  }

  retracting(movie_id: number) {
    var data: any;
    const headers = { 'Authorization': 'Bearer ' + localStorage.getItem("movieworldtoken") }
    data = {
      movie_id: movie_id
    }
    this.http
      .post<any>(
        'http://127.0.0.1:9600/api/retracting', data, { headers }
      )
      .subscribe({
        next: (res) => {
          var index = this.movies.findIndex((x: { id: number; }) => x.id == movie_id)
          if (res.message == "WAS_HATE") {
            this.movies[index].hates = this.movies[index].hates - 1;
          }
          else if (res.message == "WAS_LIKE") {
            this.movies[index].likes = this.movies[index].likes - 1;
            // this.movies[index].hates = this.movies[index].hates +1 ;
          }

          this.movies[index].reaction = null;
        },
        error: (e) => {
        },
      });
  }

  moviesByUser(user_id: number) {
    // alert(user_id);
    this.movies = [];
    this.total_number_of_movies = undefined;

    const headers = { 'Authorization': 'Bearer ' + localStorage.getItem("movieworldtoken") }
    let queryParams = new HttpParams();
    queryParams = queryParams.append("user",user_id);
    this.http
      .get<any>(
        'http://127.0.0.1:9600/api/getmovies', { headers,params:queryParams  }
      )
      .subscribe({
        next: (res) => {
          for (var i = 0; i < res.length; i++) {
            var data: any = {};
            // console.log('in loop' + res[i].title);
            data["title"] = res[i].title;
            data["description"] = res[i].description;
            data["likes"] = res[i].likes;
            data["hates"] = res[i].hates;
            data["date"] = res[i].created_at;
            data["username"] = res[i].username;
            data["user_id"] = res[i].userid;
            data["id"] = res[i].id;
            data["reaction"] = res[i].reaction;
            data["maxlikes"] = res[i].maxlikes;
            data["maxhates"] = res[i].maxhates;
            // console.log(data);
            this.movies.push(data);
          }
          this.total_number_of_movies = this.movies.length;
          // console.log(this.movies);
        },
        error: (e) => { },
        complete: () => { }
      });
  }

  moviesByFilter(filter: string) {
    // alert(filter);
    this.movies = [];
    this.total_number_of_movies = undefined;

    const headers = { 'Authorization': 'Bearer ' + localStorage.getItem("movieworldtoken") }
    let queryParams = new HttpParams();
    queryParams = queryParams.append("filter",filter);
    this.http
      .get<any>(
        'http://127.0.0.1:9600/api/getmovies', { headers,params:queryParams  }
      )
      .subscribe({
        next: (res) => {
          for (var i = 0; i < res.length; i++) {
            var data: any = {};
            // console.log('in loop' + res[i].title);
            data["title"] = res[i].title;
            data["description"] = res[i].description;
            data["likes"] = res[i].likes;
            data["hates"] = res[i].hates;
            data["date"] = res[i].created_at;
            data["username"] = res[i].username;
            data["user_id"] = res[i].userid;
            data["id"] = res[i].id;
            data["reaction"] = res[i].reaction;
            data["maxlikes"] = res[i].maxlikes;
            data["maxhates"] = res[i].maxhates;
            // console.log(data);
            this.movies.push(data);
          }
          this.total_number_of_movies = this.movies.length;
          // console.log(this.movies);
        },
        error: (e) => { },
        complete: () => { }
      });
  }

}
