import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register/register.component';
import { ListMoviesComponent } from './list-movies/list-movies.component';
import { LoginComponent } from './login/login.component';
import { NewMovieComponent } from './new-movie/new-movie.component';

import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: ListMoviesComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'listofmovies',
    component: ListMoviesComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'newmovie',
    component: NewMovieComponent,
    canActivate: [AuthGuard],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
