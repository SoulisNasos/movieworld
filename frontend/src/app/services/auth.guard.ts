import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from "@angular/router";
import { map, Observable } from "rxjs";

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {
    token: string | null | undefined;
    name: string | null | undefined;
    constructor(private router: Router) {}
    
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
        this.token =  localStorage.getItem("movieworldtoken");
        this.name =  localStorage.getItem("movieworldname");

        if(
            (this.token !== null && this.token !== null) &&
            (this.name !== 'undefined' && this.name !== 'undefined') &&
            (this.name !== undefined && this.name !== undefined)
          ){
            return true;
          }
          return this.router.createUrlTree(['/listofmovies']);



        // return this.authService.user.pipe(map(user => {
        //     const isAuth = !!user;
        //     if (isAuth) {
        //         return true;
        //     }
        //     return this.router.createUrlTree(['/listofmovies']);
        // }));
    }
}